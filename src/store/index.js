import React, { useReducer, createContext } from "react";
import io from "socket.io-client";

const initialTwitterState = {
  placeInfos: null,
  trends: [],
  tweets: [],
  location: []
};

function twitterReducer(state, action) {
  switch (action.type) {
    case "SET_PLACE_INFOS":
      return { ...state, placeInfos: action.payload };
    case "SET_LOCATION":
      return { ...state, location: [...action.payload] };
    case "SET_TRENDS":
      return { ...state, trends: [...action.payload] };
    case "SET_TWEETS":
      return { ...state, tweets: [...action.payload] };
    case "ADD_TWEET":
      state.tweets.unshift(action.payload);
      if (state.tweets.length >= 1000) {
        state.tweets.pop();
      }
      return { ...state };
    case "SETUP":
      return { ...state, ...action.payload };
    case "CLEAN_TWEETS":
      return { ...state, tweets: [] };
    default:
      throw new Error("Please specify an action in the tweeterReducer");
  }
}

const initialLanguageState = {
  language: "fr"
};

function languageReducer(state, action) {
  switch (action.type) {
    case "CHANGE_LANGUAGE":
      return { ...state, language: action.payload };
    default:
      throw new Error("Please specify an action in the languageReducer");
  }
}
const Stores = {
  lang: createContext(),
  twitter: createContext(),
  socket: io("http://localhost:80")
};

function LangProvider({ children }) {
  const [store, dispatch] = useReducer(languageReducer, initialLanguageState);
  return (
    <Stores.lang.Provider value={[store, dispatch]}>
      {children}
    </Stores.lang.Provider>
  );
}
function TwitterProvider({ children }) {
  const [store, dispatch] = useReducer(twitterReducer, initialTwitterState);
  return (
    <Stores.twitter.Provider value={[store, dispatch]}>
      {children}
    </Stores.twitter.Provider>
  );
}

const StoreProviders = {
  lang: LangProvider,
  twitter: TwitterProvider
};
const StoreConsumers = {
  lang: Stores.lang.Consumer,
  twitter: Stores.twitter.Consumer
};

export { StoreConsumers, StoreProviders, Stores };
