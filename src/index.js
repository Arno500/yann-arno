import React from "react";
import ReactDOM from "react-dom";
import Page from "./layout";
import "./styles.scss";
import "bootstrap/dist/css/bootstrap.min.css";

var mountNode = document.getElementById("app");
ReactDOM.render(<Page />, mountNode);
