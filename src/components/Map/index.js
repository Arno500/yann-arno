import React, { useRef, useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Stores } from "../../store";
import "leaflet/dist/leaflet.css";
import "./style.scss";
const Leaflet = require("react-leaflet");

const Map = ({ location }) => {
  const mapRef = useRef();
  const [setupState, setSetupState] = useState(false);
  const setTweetsLocation = () => {
    if (setupState === false) return;
    const map = mapRef.current.leafletElement;
    const bounds = map.getBounds();
    const southWest = bounds.getSouthWest();
    const northEast = bounds.getNorthEast();
    Stores.socket.emit(
      "changeLocation",
      `${southWest.lng},${southWest.lat},${northEast.lng},${northEast.lat}`
    );
  };
  useEffect(() => {
    if (location.length === 2) {
      console.log("Refreshing map position");
      const map = mapRef.current.leafletElement;
      map.setView(location, 12);
    }
  }, [location]);
  useEffect(() => {
    const map = mapRef.current.leafletElement;

    Stores.socket.on("setup", data => {
      const bounds = data.location;
      map.fitBounds([
        [bounds[1], bounds[0]],
        [bounds[3], bounds[2]]
      ]);
      setSetupState(true);
    });
  }, []);
  return (
    <div className="map">
      <Leaflet.Map
        center={[51.505, -0.09]}
        zoom={13}
        ref={mapRef}
        onMoveend={setTweetsLocation}
      >
        <Leaflet.TileLayer
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        />
      </Leaflet.Map>
    </div>
  );
};

Map.propTypes = {
  location: PropTypes.array
};

export default Map;
