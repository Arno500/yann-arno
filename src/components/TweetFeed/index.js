import React, { useContext, useEffect, memo } from "react";
import PropTypes from "prop-types";

import { Media, Card, Row } from "reactstrap";

import { Stores } from "../../store";

const TweetFeed = ({ className, tweets }) => {
  const [twitterStore, twitterDispatch] = useContext(Stores.twitter);
  useEffect(() => {
    Stores.socket.on("tweet", function(data) {
      twitterDispatch({ type: "ADD_TWEET", payload: data });
    });
    Stores.socket.on("setup", data => {
      twitterDispatch({ type: "SETUP", payload: { tweets: data.tweets } });
    });
    Stores.socket.on("cleanTweets", () => {
      twitterDispatch({ type: "CLEAN_TWEETS" });
    });
  }, []);
  return (
    <ul className={className}>
      {tweets.map(elm => (
        <li key={elm.id}>
          <a
            href={"https://twitter.com/redirect/status/" + elm.id_str}
            target="_blank"
          >
            <Row>
              <Card className="tweet">
                <Media>
                  <Media left middle>
                    <Media
                      object
                      src={elm.user.profile_image_url_https}
                      alt=""
                    />
                  </Media>
                  <Media body>
                    <Media heading>{elm.user.name} </Media>
                    {typeof elm.extended_tweet !== "undefined"
                      ? elm.extended_tweet.full_text
                      : elm.text}{" "}
                  </Media>
                </Media>
              </Card>
            </Row>
          </a>
        </li>
      ))}
    </ul>
  );
};

TweetFeed.PropTypes = {
  tweets: PropTypes.arrayOf(PropTypes.object)
};

export default memo(TweetFeed);
