import React, { useContext, useEffect, memo } from "react";
import PropTypes from "prop-types";

import { Stores } from "../../store";

const TrendsList = ({ className, trends }) => {
  const [twitterStore, twitterDispatch] = useContext(Stores.twitter);
  useEffect(() => {
    Stores.socket.on("place", function(data) {
      twitterDispatch({ type: "SET_PLACE_INFOS", payload: data });
    });
    Stores.socket.on("trends", function(data) {
      twitterDispatch({ type: "SET_TRENDS", payload: data });
    });
    Stores.socket.on("setup", function(data) {
      twitterDispatch({ type: "SET_TRENDS", payload: data.trends });
    });
  }, []);
  return (
    <ul className={className}>
      {trends.map((elm, index) => (
        <li key={index}>
          <a href={elm.url} target="_blank">
            {elm.name}
          </a>
        </li>
      ))}
    </ul>
  );
};

TrendsList.PropTypes = {
  trends: PropTypes.arrayOf(PropTypes.object)
};

export default memo(TrendsList);
