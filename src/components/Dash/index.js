import React, { useContext } from "react";
import { Stores } from "../../store";

import { Container, Row, Col } from "reactstrap";

import TweetFeed from "../TweetFeed";
import TrendsList from "../TrendsList";
import Map from "../Map";
import LocationManager from "../LocationManager";

import "./style.scss";

export default function Dash() {
  const [twitterStore, twitterDispatch] = useContext(Stores.twitter);
  return (
    <Container>
      <LocationManager dispatch={twitterDispatch} />
      <Map location={twitterStore.location} />
      <Row>
        <Col>
          <TweetFeed tweets={twitterStore.tweets} className="tweet-feed" />
        </Col>
        <Col sm="4">
          <TrendsList trends={twitterStore.trends} className="trends-list" />
        </Col>
      </Row>
    </Container>
  );
}
