import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

import { Button, Input, Form, ButtonGroup, Col, Row } from "reactstrap";

const LocationManager = ({ dispatch }) => {
  const [locationInput, setLocationInput] = useState("");
  const [geolocation, setGeolocation] = useState([]);
  const setLocation = pos => {
    if (!pos) {
      fetch(
        "https://nominatim.openstreetmap.org/?format=jsonv2&q=" + locationInput
      )
        .then(data => data.json())
        .then(json => {
          dispatch({
            type: "SET_LOCATION",
            payload: [json[0].lat, json[0].lon]
          });
        });
    } else {
      dispatch({ type: "SET_LOCATION", payload: pos });
    }
  };
  const handleSend = e => {
    e.preventDefault();
    setLocation();
  };
  const setInput = e => {
    setLocationInput(e.target.value);
  };
  const goToGeo = () => {
    setLocation(geolocation);
  };
  useEffect(() => {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(pos =>
        setGeolocation([pos.coords.latitude, pos.coords.longitude])
      );
      /* la géolocalisation est disponible */
    }
  }, []);
  return (
    <>
      <Form onSubmit={handleSend}>
        <Row>
          <Col sm="4">
            <Input
              type="text"
              placeholder="Chercher un lieu"
              value={locationInput}
              onChange={setInput}
            />
          </Col>
          <Col>
            <ButtonGroup>
              <Button onClick={goToGeo}>Géolocalisation</Button>
              <Button color="primary" type="submit" value="J'y vais !">
                J'y vais !
              </Button>
            </ButtonGroup>
          </Col>
        </Row>
      </Form>
    </>
  );
};

LocationManager.propTypes = {
  dispatch: PropTypes.func
};

export default LocationManager;
