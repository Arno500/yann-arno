import React from "react";
import { StoreProviders } from "../store";

import Dash from "../components/Dash";

const Page = () => {
  return (
    <div>
      <StoreProviders.lang>
        <StoreProviders.twitter>
          <Dash></Dash>
        </StoreProviders.twitter>
      </StoreProviders.lang>
    </div>
  );
};

export default Page;
