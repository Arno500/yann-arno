const Twitter = require("twit");
const apiData = require("./env.json");

const debounce = require("lodash/debounce");

const twitter = new Twitter({
  consumer_key: apiData.twitterApiKey,
  consumer_secret: apiData.twitterApiSecret,
  access_token: apiData.twitterAccessToken,
  access_token_secret: apiData.twitterAccessTokenSecret
});

let location = "2.199862,48.802612,2.474398,48.915190";
let placeInfos = null;

let tweetsArray = [];
let trendsArray = [];

var io = require("socket.io")(80);

let tweetsStream = null;

function startStream() {
  if (tweetsStream !== null) tweetsStream.stop();
  tweetsStream = twitter.stream("statuses/filter", {
    locations: location
  });
  tweetsStream.on("error", error => {
    console.error(error, " this is probably a rate limit, retrying in 10 secs");
    setTimeout(startStream, 10 * 1000);
  });
  tweetsStream.on("tweet", function(event) {
    if (!tweetsArray.some(elm => elm.id === event.id)) {
      io.emit("tweet", event);
      tweetsArray.unshift(event);
    }

    if (tweetsArray.length >= 1000) {
      tweetsArray.pop();
    }
  });
}

async function getFromTwitter(endpoint, input, callback) {
  return new Promise((resolve, reject) => {
    twitter.get(endpoint, input, (err, data, response) => {
      if (err) {
        console.error(err);
        reject(err);
      }
      if (callback) callback(err, data, response);
      resolve(data);
    });
  });
}

async function detectLocation() {
  const locationArray = location.split(",").map(Number);
  const long = (locationArray[0] + locationArray[2]) / 2;
  const lat = (locationArray[1] + locationArray[3]) / 2;
  await getFromTwitter("trends/closest", {
    lat,
    long
  })
    .then(data => {
      if (data[0].woeid) {
        placeInfos = data[0];
        io.emit("place", placeInfos);
      } else {
        console.error(
          "No results for location given... Are you sure everything is okay?"
        );
      }
    })
    .catch(err => {
      if (err.code !== 34) {
        setTimeout(detectLocation, 10 * 1000);
      }
    });
  await getFromTwitter("trends/place", { id: placeInfos.woeid })
    .then(data => {
      trendsArray = data[0].trends;
      io.emit("trends", trendsArray);
    })
    .catch(err => {
      if (err.code !== 34) {
        setTimeout(detectLocation, 10 * 1000);
      }
    });
}

function restartStream() {
  console.log("Restarting streaming from Twitter");
  tweetsArray = [];
  io.emit("cleanTweets");
  startStream();
  detectLocation();
}

const debouncedRestartStream = debounce(restartStream, 4000);

startStream();

io.on("connection", async socket => {
  const locationArray = location.split(",").map(Number);
  if (placeInfos === null) await detectLocation();
  io.emit("setup", {
    tweets: tweetsArray,
    location: locationArray,
    trends: trendsArray
  });
  socket.on("changeLocation", data => {
    location = data;
    debouncedRestartStream();
  });
});
